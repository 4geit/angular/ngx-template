#!/bin/bash

function check_env_variables {
  if [ "$ANGULAR_NAMESPACE_ID" == "" ]; then
    echo "ANGULAR_NAMESPACE_ID is not defined. Check the README for more details."
    exit -1
  fi
  if [ "$GITLAB_PRIVATE_TOKEN" == "" ]; then
    echo "GITLAB_PRIVATE_TOKEN is not defined. Check the README for more details."
    exit -1
  fi
  if [ "$GITLAB_AUTH_TOKEN" == "" ]; then
    echo "GITLAB_AUTH_TOKEN is not defined. Check the README for more details."
    exit -1
  fi
  if [ "$NPM_TOKEN" == "" ]; then
    echo "NPM_TOKEN is not defined. Check the README for more details."
    exit -1
  fi
}

function prompt {
  if [ "$1" == "" ]; then
    echo "Type the package name"
    read NAME
  else
    NAME=$1
  fi

  if [ "$2" == "" ]; then
    echo "Type the package type <module|component|service>"
    read TYPE
  else
    TYPE=$2
  fi

  if [ "$AUTHOR_NAME" == "" ]; then
    if [ "$3" == "" ]; then
      echo "Type author name"
      read AUTHOR_NAME
    else
      AUTHOR_NAME=$3
    fi
  fi

  if [ "$AUTHOR_EMAIL" == "" ]; then
    if [ "$4" == "" ]; then
      echo "Type author email"
      read AUTHOR_EMAIL
    else
      AUTHOR_EMAIL=$4
    fi
  fi

  echo "Type the package description"
  read DESCRIPTION
}

function set_variables {
  FILE=$1
  CLASS=$(echo ngx-$NAME-$TYPE | sed -r 's/(^|-)([a-z])/\U\2/g')
  MODULE_CLASS=$(echo ngx-$NAME-module | sed -r 's/(^|-)([a-z])/\U\2/g')
  sed -i \
    -e "s/<NAME>/$NAME/g" \
    -e "s/<TYPE>/$TYPE/g" \
    -e "s/<AUTHOR_NAME>/$AUTHOR_NAME/g" \
    -e "s/<AUTHOR_EMAIL>/$AUTHOR_EMAIL/g" \
    -e "s/<DESCRIPTION>/$DESCRIPTION/g" \
    -e "s/<CLASS>/$CLASS/g" \
    -e "s/<MODULE_CLASS>/$MODULE_CLASS/g" \
    ./ngx-$NAME-$TYPE/$FILE
}

check_env_variables

prompt $*

echo "create the new repository folder ngx-$NAME-$TYPE"
mkdir -p ngx-$NAME-$TYPE

echo "copy .gitignore"
cp ./ngx-template/template/.gitignore ./ngx-$NAME-$TYPE/

echo "copy .gitlab-ci.yml"
cp ./ngx-template/template/.gitlab-ci.yml ./ngx-$NAME-$TYPE/
set_variables .gitlab-ci.yml

echo "copy index.ts"
cp ./ngx-template/template/index.ts.$TYPE ./ngx-$NAME-$TYPE/index.ts
set_variables index.ts

echo "copy LICENSE"
cp ./ngx-template/template/LICENSE ./ngx-$NAME-$TYPE/

echo "copy package.json"
cp ./ngx-template/template/package.json ./ngx-$NAME-$TYPE/
set_variables package.json

echo "copy README.md"
cp ./ngx-template/template/README.md.$TYPE ./ngx-$NAME-$TYPE/README.md
set_variables README.md

echo "copy ngx-$NAME.$TYPE.ts"
cp ./ngx-template/template/$TYPE.ts ./ngx-$NAME-$TYPE/ngx-$NAME.$TYPE.ts
set_variables ngx-$NAME.$TYPE.ts

if [ "$TYPE" == "component" ] || [ "$TYPE" == "service" ]; then
  echo "copy ngx-$NAME.module.ts"
  cp ./ngx-template/template/module.ts.$TYPE ./ngx-$NAME-$TYPE/ngx-$NAME.module.ts
  set_variables ngx-$NAME.module.ts

  echo "copy ngx-$NAME.$TYPE.spec.ts"
  cp ./ngx-template/template/$TYPE.spec.ts ./ngx-$NAME-$TYPE/ngx-$NAME.$TYPE.spec.ts
  set_variables ngx-$NAME.$TYPE.spec.ts
fi

if [ "$TYPE" == "component" ]; then
  echo "copy ngx-$NAME.$TYPE.pug"
  cp ./ngx-template/template/$TYPE.pug ./ngx-$NAME-$TYPE/ngx-$NAME.$TYPE.pug
  set_variables ngx-$NAME.$TYPE.pug

  echo "copy ngx-$NAME.$TYPE.scss"
  cp ./ngx-template/template/$TYPE.scss ./ngx-$NAME-$TYPE/ngx-$NAME.$TYPE.scss
  set_variables ngx-$NAME.$TYPE.scss
fi

if [ "$TYPE" == "service" ]; then
  echo "copy variables.ts"
  cp ./ngx-template/template/variables.ts ./ngx-$NAME-$TYPE/
  set_variables variables.ts
fi

echo "change directory to ./ngx-$NAME-$TYPE"
cd ./ngx-$NAME-$TYPE

echo "create gitlab repository"
curl -X POST --header "PRIVATE-TOKEN: $GITLAB_PRIVATE_TOKEN" \
  --form "namespace_id=$ANGULAR_NAMESPACE_ID" \
  --form "visibility=public" \
  --form "path=ngx-$NAME-$TYPE" \
  --form "description=$DESCRIPTION" \
  https://gitlab.com/api/v4/projects > /dev/null

echo "add GITLAB_AUTH_TOKEN variable"
curl -X POST --header "PRIVATE-TOKEN: $GITLAB_PRIVATE_TOKEN" \
  --form "key=GITLAB_AUTH_TOKEN" \
  --form "value=$GITLAB_AUTH_TOKEN" \
  --form "protected=false" \
  https://gitlab.com/api/v4/projects/4geit%2Fangular%2Fngx-$NAME-$TYPE/variables > /dev/null

echo "add NPM_TOKEN variable"
curl -X POST --header "PRIVATE-TOKEN: $GITLAB_PRIVATE_TOKEN" \
  --form "key=NPM_TOKEN" \
  --form "value=$NPM_TOKEN" \
  --form "protected=false" \
  https://gitlab.com/api/v4/projects/4geit%2Fangular%2Fngx-$NAME-$TYPE/variables > /dev/null

echo "enable the CI runner $RUNNER_ID"
curl -X POST --header "PRIVATE-TOKEN: $GITLAB_PRIVATE_TOKEN" \
  --form "runner_id=$RUNNER_ID" \
  https://gitlab.com/api/v4/projects/4geit%2Fangular%2Fngx-$NAME-$TYPE/runners > /dev/null

echo "git init"
git init > /dev/null

echo "add remote repository URL"
git remote add origin git@gitlab.com:4geit/angular/ngx-$NAME-$TYPE.git

echo "git add"
git add .

echo "git commit"
git commit -m "feat(init) new repository" > /dev/null

echo "git push"
git push -u origin master > /dev/null

echo "yarn link"
yarn link

echo
echo
echo "The new repository has been properly generated and is available at https://gitlab.com/4geit/angular/ngx-$NAME-$TYPE"
echo
echo "You can clone the repository with the following command:"
echo
echo "git clone git@gitlab.com:4geit/angular/ngx-$NAME-$TYPE.git"
echo
echo "Or just get access to the locally generated folder:"
echo
echo "cd ngx-$NAME-$TYPE"
echo
