import { TestBed, inject } from '@angular/core/testing';

import { <CLASS> } from './<NAME>.<TYPE>';

describe('<CLASS>', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [<CLASS>]
    });
  });

  it('should be created', inject([<CLASS>], (service: <CLASS>) => {
    expect(service).toBeTruthy();
  }));
});
