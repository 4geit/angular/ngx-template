import { InjectionToken } from '@angular/core';

export const VAR1 = new InjectionToken<string>('var1');
export const VAR2 = new InjectionToken<string>('var2');
export const VAR3 = new InjectionToken<string>('var3');
