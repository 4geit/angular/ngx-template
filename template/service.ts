import { Inject, Injectable, Optional } from '@angular/core';

import { VAR1, VAR2, VAR3 } from './variables';

@Injectable()
export class <CLASS> {

  var1 = '';
  var2 = '';
  var3 = '';

  constructor(
    @Optional()@Inject(VAR1) var1: string
    @Optional()@Inject(VAR2) var2: string
    @Optional()@Inject(VAR3) var3: string
  ) {
    if (var1) { this.var1 = var1; }
    if (var2) { this.var2 = var2; }
    if (var3) { this.var3 = var3; }
  }

}
