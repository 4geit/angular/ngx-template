import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-<NAME>',
  template: require('pug-loader!./ngx-<NAME>.<TYPE>.pug')(),
  styleUrls: ['./ngx-<NAME>.<TYPE>.scss']
})
export class <CLASS> implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
