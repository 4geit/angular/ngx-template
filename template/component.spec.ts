import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { <CLASS> } from './ngx-<NAME>.<TYPE>';

describe('<NAME>', () => {
  let component: <NAME>;
  let fixture: ComponentFixture<<NAME>>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ <NAME> ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(<NAME>);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
