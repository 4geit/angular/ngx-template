# ngx-template [![npm version](//badge.fury.io/js/@4geit%2Fngx-template.svg)](//badge.fury.io/js/@4geit%2Fngx-template)

---

Scaffold the code base of an angular reusable module, component or service.

## Installation

1. A recommended way to install ***@4geit/ngx-template*** is through [git](//gitlab.com/4geit/angular/ngx-template) repository using the following command:

```bash
git clone git@gitlab.com:4geit/angular/ngx-template.git
```

alternatively you can use the HTTPS URL:

```bash
git clone https://gitlab.com/4geit/angular/ngx-template.git
```

2. `ngx-template` requires you to set those environment variables:

* `ANGULAR_NAMESPACE_ID`: the namespace id of the group/subgroup where you want the new project to be created (e.g. if you have the right two create a new project with the subgroup 4geit/angular, you can set it's namespace ID `1693681`)
* `GITLAB_PRIVATE_TOKEN`: a gitlab token allowed to create an account in the namespace ID (4geit/angular)
* `GITLAB_AUTH_TOKEN`: a gitlab token allowed to release tags on the created project
* `NPM_TOKEN`: a npm token allowed to publish a package in the `@4geit` organization
* `RUNNER_ID`: the runner id that will run the CI build.

If you are a BASH user, you can add those variables in your `~/.bashrc` file:

```bash
export ANGULAR_NAMESPACE_ID=1693681
export GITLAB_PRIVATE_TOKEN=XXXX
export GITLAB_AUTH_TOKEN=XXXX
export NPM_TOKEN=XXXX
export RUNNER_ID=XXXX
```

## Create a new repository

1. The easiest way to generate a new repository with `ngx-template` is to use the following command line:

```
./ngx-template/create-new-repo.sh
```

The command will ask a set of values such as:

* `<NAME>`: the name of the repository the one used in `ngx-<NAME>-<TYPE>`
* `<TYPE>`: the type of the repository the one used in `ngx-<NAME>-<TYPE>` it can contain the value `module`, `component` or `service`
* `<AUTHOR_NAME>`: the name of the author stored in the `package.json`
* `<AUTHOR_EMAIL>`: the name of the author stored in the `package.json`
* `<DESCRIPTION>`: the description of the project stored in the `package.json` and `README.md`

2. But you can also use the command argument by doing so:

```
./ngx-template/create-new-repo.sh <NAME> <TYPE> <AUTHOR_NAME> <AUTHOR_EMAIL>
```

You will still be asked to type the description though.

3. You can also skip typing the author name and email each time by setting those environment variables:

```
export AUTHOR_NAME="Master Peace"
export AUTHOR_EMAIL="master.peace@provider.xyz"
```
